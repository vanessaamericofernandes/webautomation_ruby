Given('I am on the Home Page') do
  @home_page = HomePage.new
  @home_page.load
end

Then('I see the following social media links along with their correspondent paths') do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  table.hashes.each do | value |
    expect(@home_page.send("get_#{value["social_media"]}_path")).to eq(value["social_media_path"])
    @home_page.save_screenshot("social_media_links.png")  
   end
end
 