Given('I access the Registration form') do
  @registration_page = RegistrationPage.new
  @registration_page.load
end

Given('fill the required fields with random valid data') do
  @registration_page.fill_form_with_random_user_information
end

Given('fill the required fields with valid data {string}, {string}, {string}, {string}, {string}, {string}') do |firstname, lastname, email, phone, account_type, document_number|
  @registration_page.fill_form_with_user_information(firstname, lastname, email, phone, account_type, document_number)
end

Given ('fill the required fields with {string}, {string}, {string}, {string}, {string}, {string} according to the input file') do |firstname, lastname, email, phone, account_type, document_number|
  @registration_page.fill_form_with_user_information(
    UserData.get(firstname), 
    UserData.get(lastname),
    UserData.get(email),
    UserData.get(phone), 
    UserData.get(account_type), 
    UserData.get(document_number))
end

When('I confirm for registration') do
  @registration_page.confirm_registration
end

Then('the system will show a {string}') do |success_msg|
  @registration_page.wait_until_success_msg_visible
  expect(@registration_page).to have_content success_msg
  @registration_page.save_screenshot("registration_sucess_msg.png") 
end

Then('an error message is displayed on the top of the form {string}') do |error_message|
  expect(@registration_page).to have_content error_message
  @registration_page.save_screenshot("registration_sucess_msg.png") 
end

Then('an error message is displayed at field level {string}') do |field_error_message|
  expect(@registration_page).to have_content field_error_message
  @registration_page.save_screenshot("registration_sucess_msg.png") 
end

Then('the user remains on the Registration page') do
  expect(@registration_page).to have_current_path '/qarentena/cadastro/'
  @registration_page.save_screenshot("registration_sucess_msg.png") 
end
