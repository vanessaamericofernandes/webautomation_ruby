class HomePage < SitePrism::Page
  set_url '/'
  element :link_facebook, '.social-icons > li:nth-child(1) > a'
  element :link_instagram, '.social-icons > li:nth-child(2) > a'
  element :link_linkedin, '.social-icons > li:nth-child(3) > a'
  
  def get_facebook_path
    link_facebook.visible?
    link_facebook[:href]
  end

  def get_instagram_path
    link_instagram.visible?
    link_instagram[:href]
  end

  def get_linkedin_path
    link_linkedin.visible?
    link_linkedin[:href]
  end

end