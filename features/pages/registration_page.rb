class RegistrationPage < SitePrism::Page
  set_url '/cadastro'
  element :cmp_firstname, '#field_nome'
  element :cmp_lastname, '#field_sobrenome'
  element :cmp_phone, '#field_telefone'
  element :cmp_email, '#field_email'
  element :rd_cpf, '#field_conta-0'
  element :rd_cnpj, '#field_conta-1'
  element :cmp_document, '#field_documento'
  element :btn_submit, '.frm_button_submit'
  element :success_msg, '.frm_message'
  
  def fill_form_with_random_user_information
    cmp_firstname.set Faker::Name.first_name
    cmp_lastname.set Faker::Name.last_name
    cmp_email.set Faker::Internet.email(:domain => 'test')
    cmp_phone.set Faker::Base.numerify('119########')
    rd_cpf.set true
    cmp_document.set Faker::CPF.numeric
  end

  def fill_form_with_user_information(firstname, lastname, email, phone, account_type, document_number)
    cmp_firstname.set firstname
    cmp_lastname.set lastname
    cmp_email.set email
    cmp_phone.set phone
    if account_type == 'pf'
      rd_cpf.set true
    end
    if account_type == 'pj'
      rd_cnpj.set true
    end
    cmp_document.set document_number
  end

  def confirm_registration
    btn_submit.click
  end
end