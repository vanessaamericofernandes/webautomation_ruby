@regressivo
Feature: Social Media links availability
  As a QArentena website user
  I want to see the related social medial links
  So that I can follow QArentena in all social media plataforms

  @social_media_positive_flow
  Scenario: Social media links are displayed on Home Page
    Given I am on the Home Page
    Then I see the following social media links along with their correspondent paths
      | social_media | social_media_path          |
      | facebook     | http://facebook.com/       |
      | instagram    | https://www.instagram.com/ |
      | linkedin     | https://www.linkedin.com/  |
