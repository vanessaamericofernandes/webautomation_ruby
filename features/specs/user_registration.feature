@regression
Feature: User Registration
  As a QArentena Demo site user
  I want to create a Demo Account
  So that I can receive news about Software Quality

  Background: 
    Given I access the Registration form

  @registration_positive_flow
  Scenario: Register a user using random valid data
    And fill the required fields with random valid data
    When I confirm for registration
    Then the system will show a 'Cadastro efetuado com sucesso. Obrigado'

  @registration_positive_flow
  Scenario Outline: Register a user using valid data
    And fill the required fields with valid data <firstname>, <lastname>, <email>, <phone>, <account_type>, <document_number>
    When I confirm for registration
    Then the system will show a <success_message>

    Examples: 
      | firstname | lastname | email                 | phone         | account_type | document_number  | success_message                           |
      | "Fabio"   | "Araujo" | "fabio@teste.com.br"  | "1197624321"  | "pf"         | "81278804005"    | "Cadastro efetuado com sucesso. Obrigado" |
      | "Flavio"  | "Araujo" | "flavio@teste.com.br" | "11976222444" | "pf"         | "14854676086"    | "Cadastro efetuado com sucesso. Obrigado" |
      | "Felipe"  | "Araujo" | "felipe@teste.com.br" | "11976222666" | "pj"         | "05474244000120" | "Cadastro efetuado com sucesso. Obrigado" |

  @registration_negative_flow
  Scenario Outline: The system does not allow registration with invalid or incomplete data
    And fill the required fields with <firstname>, <lastname>, <email>, <phone>, <account_type>, <document_number> according to the input file
    When I confirm for registration
    Then an error message is displayed on the top of the form <error_message>
    And an error message is displayed at field level <field_error_message>
    And the user remains on the Registration page

    Examples: 
      | firstname         | lastname         | email                  | phone                | account_type         | document_number | error_message                                                        | field_error_message |
      | "empty_firstname" | "valid_lastname" | "valid_email"          | "valid_phone"        | "pf"                 | "valid_cpf"     | "There was a problem with your submission. Errors are marked below." | "Campo obrigatório" |
      | "valid_firstname" | "empty_lastname" | "valid_email"          | "valid_phone"        | "pf"                 | "valid_cpf"     | "There was a problem with your submission. Errors are marked below." | "Campo obrigatório" |
      | "valid_firstname" | "valid_lastname" | "empty_email"          | "valid_phone"        | "pf"                 | "valid_cpf"     | "There was a problem with your submission. Errors are marked below." | "Campo obrigatório" |
      | "valid_firstname" | "valid_lastname" | "email_without_atsign" | "valid_phone"        | "pf"                 | "valid_cpf"     | ""                                                                   | ""                  |
      | "valid_firstname" | "valid_lastname" | "valid_email"          | "phone_with_letters" | "pj"                 | "valid_cnpj"    | ""                                                                   | ""                  |
      | "valid_firstname" | "valid_lastname" | "valid_email"          | "valid_phone"        | "empty_account_type" | "valid_cnpj"    | "There was a problem with your submission. Errors are marked below." | "Campo obrigatório" |
      | "valid_firstname" | "valid_lastname" | "valid_email"          | "valid_phone"        | "pj"                 | "empty_cnpj"    | "There was a problem with your submission. Errors are marked below." | "Campo obrigatório" |